# チェック先
- [PyPI](https://pypi.org/)
- [nodesource](https://github.com/nodesource/distributions#debinstall)
- [pyenv anaconda version](https://github.com/pyenv/pyenv/tree/master/plugins/python-build/share/python-build)
- [pyenv version](https://github.com/pyenv/pyenv/releases)
- [torch stable wheel file](https://download.pytorch.org/whl/torch_stable.html)
- torch nightly
  - [cuda11.1.x](https://download.pytorch.org/whl/nightly/cu111/torch_nightly.html)
  - [cuda11.3.x](https://download.pytorch.org/whl/nightly/cu113/torch_nightly.html)
- [code-server](https://github.com/cdr/code-server)
- [rclone](https://github.com/rclone/rclone)
- [cupy-cuda](https://github.com/cupy/cupy)

# バージョン

## Stable

- [ ] pyenv: 
- [ ] nodejs: 
- [ ] anaconda3: 
- [ ] opencv-python: 
- [ ] tensorflow-gpu: 
- [ ] keras: 
- [ ] torch: 
- [ ] torchvision: 
- [ ] torchsummary: 
- [ ] jupyterlab: 
- [ ] cupy-cuda: 
- [ ] code-server: 
- [ ] rclone: 

## Feature

- [ ] pyenv: 
- [ ] nodejs: 
- [ ] anaconda3: 
- [ ] opencv-python: 
- [ ] tf-nightly-gpu: 
- [ ] keras: 
- [ ] torch-nightly: 
- [ ] torchvision-nightly: 
- [ ] torchsummary: 
- [ ] jupyterlab: 
- [ ] cupy-cuda: 
- [ ] code-server: 
- [ ] rclone: 
